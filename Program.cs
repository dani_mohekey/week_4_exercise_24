﻿using System;

namespace week_4_exercise_24
{
    class Program
    {
        static void PrintGreeting( )
        {
            var a = $"My first method";
            Console.WriteLine(a);
        }
        
        static void Main(string[] args)
        {
            PrintGreeting();
            PrintGreeting();
            PrintGreeting();
            PrintGreeting();
            PrintGreeting();
        }
   
   
    }
}
